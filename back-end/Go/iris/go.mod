module blog

go 1.13

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/aliyun/alibaba-cloud-sdk-go v0.0.0-20191029065742-0c006f3860af
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fasthttp-contrib/websocket v0.0.0-20160511215533-1f3b11f56072 // indirect
	github.com/go-check/check v1.0.0-20180628173108-788fd7840127 // indirect
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.16.0
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/iris-contrib/middleware/cors v0.0.0-20191028172159-41f72a73786a
	github.com/iris-contrib/middleware/csrf v0.0.0-20191028172159-41f72a73786a
	github.com/iris-contrib/middleware/jwt v0.0.0-20191028172159-41f72a73786a
	github.com/jinzhu/gorm v1.9.11
	github.com/json-iterator/go v1.1.8
	github.com/juju/errors v0.0.0-20190930114154-d42613fe1ab9 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kataras/golog v0.0.9
	github.com/kataras/iris/v12 v12.0.1
	github.com/klauspost/compress v1.9.1 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/mitchellh/mapstructure v1.1.2
	github.com/mojocn/base64Captcha v0.0.0-20190801020520-752b1cd608b2
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/oschwald/geoip2-golang v1.3.0
	github.com/oschwald/maxminddb-golang v1.5.0 // indirect
	github.com/pelletier/go-toml v1.6.0
	github.com/qiniu/api.v7 v0.0.0-20190520053455-bea02cd22bf4
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/shiyanhui/hero v0.0.2
	github.com/tidwall/gjson v1.3.2
	github.com/valyala/fasthttp v1.6.0 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
	golang.org/x/crypto v0.0.0-20191029031824-8986dd9e96cf
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
	golang.org/x/net v0.0.0-20191028085509-fe3aa8a45271 // indirect
	golang.org/x/sys v0.0.0-20191029155521-f43be2a4598c // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/ini.v1 v1.49.0 // indirect
)
